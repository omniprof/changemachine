package com.kenfogel.bean;

/**
 *
 * @author Ken Fogel
 */
public class ChangeBean {

    private double amountToChange;
    private int twenties;
    private int tens;
    private int fives;
    private int toonies;
    private int loonies;
    private int quarters;
    private int dimes;
    private int nickels;
    private int pennies;

    public double getAmountToChange() {
        return amountToChange;
    }

    public void setAmountToChange(double amountToChange) {
        this.amountToChange = amountToChange;
    }

    public int getTwenties() {
        return twenties;
    }

    public void setTwenties(int twenties) {
        this.twenties = twenties;
    }

    public int getTens() {
        return tens;
    }

    public void setTens(int tens) {
        this.tens = tens;
    }

    public int getFives() {
        return fives;
    }

    public void setFives(int fives) {
        this.fives = fives;
    }

    public int getToonies() {
        return toonies;
    }

    public void setToonies(int toonies) {
        this.toonies = toonies;
    }

    public int getLoonies() {
        return loonies;
    }

    public void setLoonies(int loonies) {
        this.loonies = loonies;
    }

    public int getQuarters() {
        return quarters;
    }

    public void setQuarters(int quarters) {
        this.quarters = quarters;
    }

    public int getDimes() {
        return dimes;
    }

    public void setDimes(int dimes) {
        this.dimes = dimes;
    }

    public int getNickels() {
        return nickels;
    }

    public void setNickels(int nickels) {
        this.nickels = nickels;
    }

    public int getPennies() {
        return pennies;
    }

    public void setPennies(int pennies) {
        this.pennies = pennies;
    }

}
