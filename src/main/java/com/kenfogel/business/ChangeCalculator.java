package com.kenfogel.business;

import com.kenfogel.bean.ChangeBean;

/**
 *
 * @author Ken Fogel
 */
public class ChangeCalculator {

    private final ChangeBean changeBean;

    public ChangeCalculator(ChangeBean changeBean) {
        this.changeBean = changeBean;
    }

    public void makeChange() {
        int pennies = (int) (changeBean.getAmountToChange() * 100.0);

        // $20
        int twenties = pennies / 2000;
        changeBean.setTwenties(twenties);
        pennies %= 2000;

        // $10
        int tens = pennies / 1000;
        changeBean.setTens(tens);
        pennies %= 1000;

        // $5
        int fives = pennies / 500;
        changeBean.setFives(fives);
        pennies %= 500;

        // $2
        int toonies = pennies / 200;
        changeBean.setToonies(toonies);
        pennies %= 200;

        // $1
        int loonies = pennies / 100;
        changeBean.setLoonies(loonies);
        pennies %= 100;

        // $0.25
        int quarters = pennies / 25;
        changeBean.setQuarters(quarters);
        pennies %= 25;

        // $0.10
        int dimes = pennies / 10;
        changeBean.setDimes(dimes);
        pennies %= 10;

        // $0.05
        int nickels = pennies / 5;
        changeBean.setNickels(nickels);
        pennies %= 5;

        // $0.01
        changeBean.setPennies(pennies);
    }
}
