package com.kenfogel.presentation;

import com.kenfogel.bean.ChangeBean;
import java.util.Scanner;

/**
 *
 * @author Ken Fogel
 */
public class UserInterface {

    private final ChangeBean changeBean;
    private final Scanner sc;

    public UserInterface(ChangeBean changeBean) {
        sc = new Scanner(System.in);
        this.changeBean = changeBean;
    }

    public void requestAmount() {
        double amount = 0.0;
        do {
            System.out.println("How much to turn into change? ");
            if (sc.hasNextDouble()) {
                amount = sc.nextDouble();
                changeBean.setAmountToChange(amount);
            } else {
                System.out.println("Invalid input.");
            }
            sc.nextLine();
        } while (amount == 0);
    }

    public void displayTheChange() {
        System.out.println("$20 = " + changeBean.getTwenties());
        System.out.println("$10 = " + changeBean.getTens());
        System.out.println("$5 = " + changeBean.getFives());
        System.out.println("$2 = " + changeBean.getToonies());
        System.out.println("$1 = " + changeBean.getLoonies());
        System.out.println("$0.25 = " + changeBean.getQuarters());
        System.out.println("$0.10 = " + changeBean.getDimes());
        System.out.println("$0.05 = " + changeBean.getNickels());
        System.out.println("$0.01 = " + changeBean.getPennies());
    }

}
