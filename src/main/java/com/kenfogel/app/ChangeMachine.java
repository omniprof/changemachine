package com.kenfogel.app;

import com.kenfogel.bean.ChangeBean;
import com.kenfogel.business.ChangeCalculator;
import com.kenfogel.presentation.UserInterface;

/**
 *
 * @author Ken Fogel
 */
public class ChangeMachine {

    private final ChangeBean changeBean;

    private final UserInterface userInterface;
    private final ChangeCalculator changeCalculator;

    public ChangeMachine() {
        changeBean = new ChangeBean();
        userInterface = new UserInterface(changeBean);
        changeCalculator = new ChangeCalculator(changeBean);

    }

    public void perform() {
        userInterface.requestAmount();
        changeCalculator.makeChange();
        userInterface.displayTheChange();

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ChangeMachine cm = new ChangeMachine();
        cm.perform();
        System.exit(0);
    }

}
